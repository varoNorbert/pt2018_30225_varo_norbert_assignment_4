import java.io.Serializable;
import java.util.Observable;

public abstract class Cont extends Observable implements Serializable{

	private int accountNr=0;
	protected int accountValue=0;
	private int owner=0;
	
	public Cont(int nr,int val, int id) 
	{
		this.setAccountNr(nr);
		this.accountValue=val;
		this.setOwner(id);
	}

	public int getAccountNr() {
		return accountNr;
	}

	public void setAccountNr(int accountNr) {
		setChanged();
		notifyObservers("Account nr has been changed from "+this.accountNr+" to "+accountNr);
		this.accountNr = accountNr;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		setChanged();
		notifyObservers("Account "+ getAccountNr() +" owner has been changed from "+this.owner+" to "+owner);
		this.owner = owner;
	}

	public int getAccountValue() {
		return accountValue;
	}
	
	
	public void depune(int accountValue) {
		setChanged();
		notifyObservers("Account "+this.accountNr+" balance updated from "+this.accountValue+" to "+this.accountValue+accountValue);
		this.accountValue += accountValue;
	}
	
	public abstract void retrage(int amount);

	
	
}
