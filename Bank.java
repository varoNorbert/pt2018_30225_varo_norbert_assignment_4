import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;

import javax.swing.JOptionPane;

public class Bank implements BankProc{
	private HashMap<Holder, ArrayList<Cont>>  bankData=new HashMap<Holder,ArrayList<Cont>>();
	
	@SuppressWarnings("unchecked")
	public Bank() 
	{
	try{
		FileInputStream fileIn = new FileInputStream("backup.ser");
		ObjectInputStream in = new ObjectInputStream(fileIn);
		bankData = (HashMap<Holder, ArrayList<Cont>>) in.readObject();
		in.close();
		fileIn.close();
	}
	catch(ClassNotFoundException c){
		c.printStackTrace();
	}
	catch(IOException i){
		i.printStackTrace();
	}
	finally 
	{
		
	}
		
	}
	
	
	public HashMap<Holder, ArrayList<Cont>> getBankData() 
	{
		return this.bankData;
	}
	

	public Cont findCont(int id) {
		Cont rez = null;
		for(Holder p: bankData.keySet()) 
		{
			for(int i=0;i<bankData.get(p).size();i++) 
			{
				if(bankData.get(p).get(i).getAccountNr()==id)
					rez=bankData.get(p).get(i);
			}
		} 
		return rez;
	}

	@SuppressWarnings("unchecked")
	public void addCont(Holder pers,  Cont ac) {
		
		
		try{
			FileInputStream fileIn = new FileInputStream("backup.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			bankData = (HashMap<Holder, ArrayList<Cont>>) in.readObject();
			in.close();
			fileIn.close();
		}
		catch(ClassNotFoundException c){
			c.printStackTrace();
		}
		catch(IOException i){
			i.printStackTrace();
		}
		finally {}
		
		boolean insert = false;
		assert validate(ac);
		Holder accHolder=findPerson(pers.getpId());
		if(findCont(ac.getAccountNr()) == null)
				if(accHolder!=null) {
					ac.addObserver(accHolder);
					bankData.get(accHolder).add(ac);
					pers.update(ac, "Account nr " + ac.getAccountNr() + " added to client "+accHolder.getpId());
					JOptionPane.showMessageDialog(null, "Account added succesfully!");
					insert=true;
			}
			else
				JOptionPane.showMessageDialog(null, "The person doesn't exist!");
		else
			JOptionPane.showMessageDialog(null, "The account alredy exists!");
		assert insert == true;
		writeObject(bankData);
	}

	public void removeAccount(int id) {
		try{
			FileInputStream fileIn = new FileInputStream("backup.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			bankData = (HashMap<Holder, ArrayList<Cont>>) in.readObject();
			in.close();
			fileIn.close();
		}
		catch(ClassNotFoundException c){
			c.printStackTrace();
		}
		catch(IOException i){
			i.printStackTrace();
		}
		Cont acc=findCont(id);
		Holder p=findPerson(acc.getOwner());
		boolean deleted = false;
		assert id > 0;
		    try {
			for(Cont a: bankData.get(p)) {
				if(a.getAccountNr() == id) {
					
					bankData.get(p).remove(a);
					
						deleted = true;
						JOptionPane.showMessageDialog(null, "The account has been removed with success!");
						p.update(a, "Account with id " + a.getAccountNr() + " removed from person "+p.getpId());
					}
						
				}
		    }
		    catch(ConcurrentModificationException e) 
		    {}
		
		assert deleted == true;
		writeObject(bankData);
	}

	@SuppressWarnings("unchecked")
	public void editCont(Holder pers,Cont ac) {
		Cont ac1=findCont(ac.getAccountNr());
		if(pers!=null && ac1!=null) {
		if(validate(ac) && validate(pers)) {
		try{
			FileInputStream fileIn = new FileInputStream("backup.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			bankData = (HashMap<Holder, ArrayList<Cont>>) in.readObject();
			in.close();
			fileIn.close();
		}
		catch(ClassNotFoundException c){
			c.printStackTrace();
		}
		catch(IOException i){
			i.printStackTrace();
		}
		finally 
		{}
		boolean ok=false;
		assert validate(pers) && validate(ac);
		if(bankData.get(pers) != null) {
			for(int i=0;i<bankData.get(pers).size();i++) {
				if(bankData.get(pers).get(i).getAccountNr() == ac.getAccountNr()) {
					bankData.get(pers).get(i).setAccountNr(ac.getAccountNr());
				
					bankData.get(pers).get(i).setOwner(ac.getOwner());
					JOptionPane.showMessageDialog(null, "The account has been edited with success!");
					pers.update(bankData.get(pers).get(i), "Account nr " + bankData.get(pers).get(i).getAccountNr() + " of Clien id "+pers.getpId() + " updated succesfully.");
					ok=true;
				}
				else 
				{
					JOptionPane.showMessageDialog(null, "The client has no account with that account number!");

				}
			}
			
			writeObject(bankData);
			}
		assert ok;
		}
		else JOptionPane.showMessageDialog(null,"Input error");
		}
		else JOptionPane.showMessageDialog(null,"Client or Account  does not exist");
		
	}
	
	

	public void depunere(int amount, int cont, Holder person) {
	//	data = readBank();
		if(person!=null) {
		assert (amount > 0) && (cont >0);
		boolean added = false;
		for(Holder p:bankData.keySet())
		{
			for(Cont a: bankData.get(p)) {
				if(a.getAccountNr() == cont) {
					
					a.depune(amount);
					
						JOptionPane.showMessageDialog(null, amount+" ron added to account nr "+ cont+" from client with id "+person.getpId());
						person.update(a, amount+" ron added to account nr "+ cont+ " from person with id "+person.getpId());
					}
				}
		}
		
		
		writeObject(bankData);
		}
		else JOptionPane.showMessageDialog(null, "The client does not exist!");

	}

	@SuppressWarnings("unchecked")
	public void retragere(int amount, int cont, Holder p) {
		
		if(p!=null) {
		try{
			FileInputStream fileIn = new FileInputStream("backup.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			bankData = (HashMap<Holder, ArrayList<Cont>>) in.readObject();
			in.close();
			fileIn.close();
		}
		catch(ClassNotFoundException c){
			c.printStackTrace();
		}
		catch(IOException i){
			i.printStackTrace();
		}
		finally 
		{}
		assert (amount > 0) && (cont >0);
		int ok=0;
		for(Holder p1:bankData.keySet()) {
			for(Cont a: bankData.get(p1)) {
				if(a.getAccountNr() == cont && a.getOwner()==p.getpId()) {
						
						a.retrage(amount);
						JOptionPane.showMessageDialog(null, "The amount has been withdrawn!");
						p1.update(a, amount + " ron withdrawn from " + a.getAccountNr() + " of person "+p1.getpId());
						ok=1;
						
				}
			}
			}
		if(ok==0)JOptionPane.showMessageDialog(null, "The account doesn't exist or it does not belong to the owner specified!");

		writeObject(bankData);}
		else JOptionPane.showMessageDialog(null, "The client does not exist!");
	}

	public void editPerson(int id, Holder pers) {
		boolean ok=true;
		assert findPerson(id)==null;
		if(findPerson(id)==null) 
		{
			JOptionPane.showMessageDialog(null, "Client does not exist!");
			ok=false;
		}
		else {
		findPerson(id).setName(pers.getName());
		findPerson(id).setPhoneNumber(pers.getPhoneNumber());
		JOptionPane.showMessageDialog(null, "Client with id "+id+"edited !");
		writeObject(bankData);
		}
		assert(!ok);
	}
	
	public void deletePerson(int id) 
	{
		boolean ok=false;
		assert id>0;
		Holder h=findPerson(id);
		try {
		for(Holder p:bankData.keySet()) 
		{
			if(p.getpId()==h.getpId()) 
			{
				bankData.remove(p);
				JOptionPane.showMessageDialog(null, "Client with id "+ id+ " and it's accounts removed");
				ok=true;
				writeObject(bankData);
			}
		}
		}
		catch (ConcurrentModificationException e) {}
		finally {}
		assert (ok);
		
	}
	@SuppressWarnings("unchecked")
	public void addPerson(Holder p) {
		try{
			FileInputStream fileIn = new FileInputStream("backup.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			bankData = (HashMap<Holder, ArrayList<Cont>>) in.readObject();
			in.close();
			fileIn.close();
		}
		catch(ClassNotFoundException c){
			c.printStackTrace();
		}
		catch(IOException i){
			i.printStackTrace();
		}
		finally 
		{
			
		}

		assert validate(p);
		if(validate(p)) {
		int ok=0;
		if(findPerson(p.getpId())!=null) {
		JOptionPane.showMessageDialog(null, "The person exists!");
		ok=1;
		}
		if(ok==0) {
		ArrayList <Cont> newCont = new ArrayList<Cont>();
		bankData.put(p, newCont);
	
		//assert data.size() == oldHashSize + 1;
		writeObject(bankData);
		JOptionPane.showMessageDialog(null, "The person was registered with success!");}
		}
		else 
		{
			JOptionPane.showMessageDialog(null, "Input error!");
		}
		}

	

	public Holder findPerson(int id) {
		assert id > 0;
		for(Holder i: bankData.keySet())
			if(i.getpId() == id) {
				return i;
		}
		return null;
	}
	
	public void writeObject(HashMap<Holder, ArrayList<Cont>> data) {
		try{
			FileOutputStream fileOut = new FileOutputStream("backup.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(data);
			out.close();
			fileOut.close();
		}
		catch(IOException i){
			i.printStackTrace();
		}
		finally 
		{
			
		}
	}
	
	@Override
	public String toString() {
		String s = null;
		for(Holder p: bankData.keySet())
			for(Cont a: bankData.get(p))
				s += p.toString() + ", " + a.toString();
		return s;
	}
	
	



	@Override
	public boolean validate(Holder pers) {
		// TODO Auto-generated method stub
		return ValidateData.validateClient(pers);
	}

	@Override
	public boolean validate(Cont ac) {
		// TODO Auto-generated method stub
		return ValidateData.validateCont(ac);
	}







}
