import java.io.Serializable;
import java.util.ArrayList;

public interface BankProc extends Serializable{
	
	public Cont findCont(int id);
	
	
	public void addCont (Holder person, Cont ac);
	
	public void editCont (Holder p, Cont ac);
	


	public void depunere(int amount, int accountID, Holder pers);
	
	public void retragere(int amount, int accountID, Holder pers);
	
	public void editPerson (int id, Holder pers);
	
	public Holder findPerson (int id);
	

	public void addPerson (Holder pers);
	
	
	public boolean validate(Holder pers);

	public boolean validate(Cont ac);
}