import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GraphicInterface extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JLabel msg1;
	private JLabel visa;
	private JLabel mcard;
	JButton addClient=new JButton("Add Client");
	JButton addCont=new JButton("Add Account");
	JButton editClient=new JButton("Edit Existing Clients");
	JButton editCont=new JButton("Add Money");
	JButton withdrawMoney=new JButton("Withdraw");
	JButton deleteClient=new JButton("Delete Client");
	JButton deleteCont=new JButton("Delete Cont");
	JButton showClient=new JButton("Show Existing Clients");
	JButton showCont=new JButton("Show Accounts");
	AbstractFrame frame;
	public GraphicInterface() 
	{
		contentPane=new JPanel();
		
		msg1=new JLabel("Welcome to Piratus Bank");
		visa =new JLabel();
		visa.setIcon(new ImageIcon("visa.png"));
		mcard=new JLabel();
		mcard.setIcon(new ImageIcon("mcard.png"));
		
		this.setTitle("Piratus Bank");
		this.setIconImage((new ImageIcon("icon.png")).getImage());
		contentPane.setLayout(new GridBagLayout());
		
		
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.anchor=GridBagConstraints.CENTER;
		c.gridwidth = 1;
		c.weightx = 0.5;
		c.insets = new Insets(10,10,10,10);
		c.fill = GridBagConstraints.HORIZONTAL;
		
		c.gridx=0;
		contentPane.add(visa,c);
	
		c.gridx = 1;
		c.gridy = 0;
			
		contentPane.add(msg1,c);
		
		c.gridx=2;
		contentPane.add(mcard,c);
		
		c.gridx=0;
		c.gridy=1;
		
		contentPane.add(addClient,c);
		
		c.gridx=1;
		c.gridy=1;
		
		contentPane.add(addCont,c);
		
		c.gridx=0;
		c.gridy=2;
		
		contentPane.add(editClient,c);
		
		c.gridx=1;
		c.gridy=2;
		
		contentPane.add(editCont,c);
		
		c.gridx=0;
		c.gridy=3;
		
		contentPane.add(showClient,c);
		
		c.gridx=1;
		c.gridy=3;
		
		contentPane.add(showCont,c);
		c.gridx=2;
		c.gridy=1;
		
		contentPane.add(deleteClient,c);
		c.gridx=2;
		c.gridy=2;
		
		contentPane.add(withdrawMoney,c);
		c.gridx=2;
		c.gridy=3;
		
		contentPane.add(deleteCont,c);
		
		addClient.addActionListener(this);
		showClient.addActionListener(this);
		editClient.addActionListener(this);
		addCont.addActionListener(this);
		showCont.addActionListener(this);
		editCont.addActionListener(this);
		deleteCont.addActionListener(this);
		deleteClient.addActionListener(this);
		withdrawMoney.addActionListener(this);
		
		this.setContentPane(contentPane);
		this.pack();
		this.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==addClient) 
		{
			frame=new AbstractFrame(1);
		}
		if(e.getSource()==addCont) 
		{
			frame=new AbstractFrame(2);
		}
		if(e.getSource()==editClient) 
		{
			frame=new AbstractFrame(3);
		}
		if(e.getSource()==editCont) 
		{
			frame=new AbstractFrame(4);
		}
		if(e.getSource()==showClient) 
		{
			frame=new AbstractFrame(5);
		}
		if(e.getSource()==showCont) 
		{
			frame=new AbstractFrame(6);
		}
		if(e.getSource()==deleteCont) 
		{
			frame=new AbstractFrame(7);
		}
		
		if(e.getSource()==deleteClient) 
		{
			frame=new AbstractFrame(8);
		}
		
		if(e.getSource()==withdrawMoney) 
		{
			frame=new AbstractFrame(9);
		}
		
		
	}

}
