import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class AbstractFrame extends JFrame implements ActionListener
{
	private JPanel contentPane;
	JButton submit;
	ArrayList<JTextField> inputs=new ArrayList<JTextField>();
	Bank banca=new Bank();
	JTable table;
	public AbstractFrame(int arg) 
	{
		contentPane=new JPanel();
		this.setIconImage((new ImageIcon("icon.png")).getImage());
		contentPane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		c.anchor=GridBagConstraints.CENTER;
		c.gridwidth = 1;
		c.weightx = 0.5;
		c.insets = new Insets(10,10,10,10);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx=0;
		c.gridy=0;
		
		switch(arg)
		{
		case 1: this.setTitle("Add new client");
				inputs.removeAll(inputs);
				inputs.add(new JTextField("id"));
				inputs.add(new JTextField("nume"));
				inputs.add(new JTextField("telefon"));
				submit=new JButton("Add client");
				break;

		case 2: this.setTitle("Add new account");
				inputs.removeAll(inputs);
				inputs.add(new JTextField("nr account"));
				inputs.add(new JTextField("starting_value"));
				inputs.add(new JTextField("savings/spendings"));
				inputs.add(new JTextField("owner_id"));
				submit=new JButton("Add account");
				break;
		case 3: this.setTitle("Edit existing client");
				inputs.removeAll(inputs);
				inputs.add(new JTextField("id"));
				inputs.add(new JTextField("nume"));
				inputs.add(new JTextField("telefon"));
				submit=new JButton("Edit client");
				break;
		case 4: this.setTitle("Add money");
				submit=new JButton("Add money");
				inputs.removeAll(inputs);
				inputs.add(new JTextField("nr account"));
				inputs.add(new JTextField("value"));
				inputs.add(new JTextField("owner_id"));
				break;
		       case 5: this.setTitle("Show clients");
				
				table=new JTable();
				String[] s=new String[3];
				s[0]="name";
				s[1]="phoneNumber";
				s[2]="pId";
				DefaultTableModel model = new DefaultTableModel(s, 0);
				HashMap<Holder, ArrayList<Cont>>  bankData=banca.getBankData();
				Object[] toAdd1=new Object[3];
				for(Holder j:bankData.keySet()) 
				{
					toAdd1[0]=j.getName();

					toAdd1[1]=j.getPhoneNumber();

					toAdd1[2]=j.getpId();
					model.addRow(toAdd1);
				}
				table.setModel(model);
			
				c.gridy=0;
				
			
				contentPane.add(table,c);
				c.gridy=1;
				submit=new JButton("Close");
				
				
			
				break;
		case 6: this.setTitle("Show accounts");
		
			table=new JTable();
			String[] s1=new String[3];
			s1[0]="accountNr";
			s1[1]="accountValue";
			s1[2]="client";
			DefaultTableModel model1 = new DefaultTableModel(s1, 0);
			HashMap<Holder, ArrayList<Cont>>  bankData1=banca.getBankData();
			Object[] toDisplay=new Object[3];
			for(Holder j:bankData1.keySet()) 
			{
			for(Cont a:bankData1.get(j)) 
			{
			toDisplay[0]=a.getAccountNr();
			toDisplay[1]=a.getAccountValue();
			toDisplay[2]=a.getOwner();
			model1.addRow(toDisplay);
			}
			
			}
			table.setModel(model1);
	
			c.gridy=0;
			
	
			contentPane.add(table,c);
			c.gridy=1;
				
				submit=new JButton("Close");
			break;
				
		case 7:
			inputs.removeAll(inputs);
			inputs.add(new JTextField("nr account"));
			this.setTitle("Show accounts");
			submit=new JButton("Delete Account");
			break;
			
		case 8:

			inputs.removeAll(inputs);
			inputs.add(new JTextField("id client"));
			this.setTitle("Remove client");
			submit=new JButton("Remove client");
			break;
		case 9:
			this.setTitle("Withdraw Money");
			inputs.removeAll(inputs);
			inputs.add(new JTextField("nr account"));
			inputs.add(new JTextField("value"));
			inputs.add(new JTextField("owner_id"));
			submit=(new JButton("Withdraw money"));
			break;
		
		}
		
		for(int i=0;i<inputs.size();i++) {
			contentPane.add(inputs.get(i),c);
			c.gridy++;
		}
		setPreferredSize(new Dimension(300,300));
		submit.setPreferredSize(new Dimension(200,30));
		submit.addActionListener(this);
		contentPane.add(submit,c);
		

		this.setContentPane(contentPane);
		this.pack();
		this.setVisible(true);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==submit) 
		{
			
			String s1,s2,s3,s4;
			switch(submit.getText()) 
			{
			
			case "Add client":
					s1=inputs.get(0).getText();
					s2=inputs.get(1).getText();
					s3=inputs.get(2).getText();
					Holder toAdd=new Holder(s2,s3,Integer.parseInt(s1));
					banca.addPerson(toAdd);
					this.dispose();
					break;
			
			case "Add account":
			s1=inputs.get(0).getText();
			s2=inputs.get(1).getText();
			s3=inputs.get(2).getText();
			s4=inputs.get(3).getText();
			Holder accHolder=banca.findPerson(Integer.parseInt(s4));
			Cont acc;
			if(s3.equals("savings")) {
			acc = new Savings(Integer.parseInt(s1),Integer.parseInt(s2),Integer.parseInt(s4));
			}
			else
			acc = new Spendings(Integer.parseInt(s1),Integer.parseInt(s2),Integer.parseInt(s4));
			banca.addCont(accHolder,acc);
			this.dispose();
			break;
			
			case "Edit client":
			
				s1=inputs.get(0).getText();
				s2=inputs.get(1).getText();
				s3=inputs.get(2).getText();
				Holder toEdit=new Holder(s2,s3,Integer.parseInt(s1));
				banca.editPerson(Integer.parseInt(s1), toEdit);
				this.dispose();
			break;
			

			case "Add money":
				s1=inputs.get(0).getText();
				s2=inputs.get(1).getText();
				s3=inputs.get(2).getText();
				Holder accHolder1=banca.findPerson(Integer.parseInt(s3));
				banca.depunere(Integer.parseInt(s2),Integer.parseInt(s1),accHolder1);
				this.dispose();
			break;
			
			case "Close":
			this.dispose();
			break;
			case "Delete Account":
				s1=inputs.get(0).getText();
			
				banca.removeAccount(Integer.parseInt(s1));
				this.dispose();
				break;
				
			case "Remove client":
			s1=inputs.get(0).getText();
			banca.deletePerson(Integer.parseInt(s1));
			this.dispose();
			break;
			case "Withdraw money":
				s1=inputs.get(0).getText();
				s2=inputs.get(1).getText();
				s3=inputs.get(2).getText();
				Holder accHolder2=banca.findPerson(Integer.parseInt(s3));
				banca.retragere(Integer.parseInt(s2),Integer.parseInt(s1),accHolder2);
				this.dispose();
				
			break;
			
			}
			
		}
		
	}

}
