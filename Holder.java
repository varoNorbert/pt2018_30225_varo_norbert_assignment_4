import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Holder implements Observer,Serializable{
	
	private String name;
	private String phoneNumber;
	private int pId;
	
	
	public Holder(String name,String phone, int pID) 
	{
		this.name=name;
		this.phoneNumber=phone;
		this.pId=pID;
	}
	public boolean equals(Object o){
		
		if(this == (Holder)o)
			return true;
		return false;
	}
	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		System.out.println(arg1);
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getpId() {
		return pId;
	}


	public void setpId(int pId) {
		this.pId = pId;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
