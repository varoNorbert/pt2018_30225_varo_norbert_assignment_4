
public class Savings extends Cont{

	public Savings(int nr, int val, int id) {
		super(nr, (int)(val+val*0.06), id);
		// TODO Auto-generated constructor stub
	}
	
	public void depune(int amount) 
	{
		setChanged();
		notifyObservers("Account "+getAccountNr()+" balance updated from "+getAccountValue()+" to "+getAccountValue()+(int)(1.06*amount));
		this.accountValue += getAccountValue()+(int)(1.06*amount);
	}
	
	@Override
	public void retrage(int amount) {
		// TODO Auto-generated method stub
		if(amount>100) 
		{
			if(amount<=getAccountValue()) 
			{
				super.depune(-amount);
			}
			
		}
	}

}
