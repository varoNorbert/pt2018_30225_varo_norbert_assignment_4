import java.util.regex.Pattern;

public class ValidateData {

	public static boolean validateClient(Holder h) 
	{
		boolean nameNumber=false;
		boolean idOk=true;
		boolean phone=true;
		nameNumber=Pattern.compile( "[0-9]" ).matcher( h.getName() ).find();
		if(h.getpId()<=0)idOk=false;
		if(h.getPhoneNumber().length()!=10)phone=false;
		return nameNumber&&idOk&&phone;
		
	}
	
	public static boolean validateCont(Cont h) 
	{
		
		boolean idOk=true;
		boolean ownerOk=true;
		if(h.getAccountNr()<=0)idOk=false;
		if(h.getOwner()<=0)ownerOk=false;
		return idOk&&ownerOk;
		
	}
}
